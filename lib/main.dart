//*****************************************************
//David Masangcay
//CS481
//Homework 2
//9/14/2020
//*****************************************************
import 'dart:ffi';

import 'package:flutter/material.dart';

void main() {runApp(MyApp());}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container
                  (
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Peanut Butter Jelly Sandwhich',
                      style: TextStyle(fontWeight: FontWeight.bold,)),
                ),
                Icon(Icons.star, color: Colors.yellow[500]),
                Text('4.95/5', style: TextStyle(color: Colors.black,))
              ],
            ),
          ),

          _buildButtonColumn(
              Colors.black, Icons.list, 'David Masangcays\n          recipies'),
        ],
      ),
    );

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(
              Colors.blue[500], Icons.forward, 'Click for recipe'),
          _buildButtonColumn(
              Colors.blue[500], Icons.access_time, 'Save for later'),
          _buildButtonColumn(
              Colors.blue[500], Icons.thumb_up, 'Like this recipe'),
          _buildButtonColumn(
              Colors.blue[500], Icons.rate_review, 'Leave a rating'),
        ],
      ),
    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'The amazing Peanut Butter Jelly Sandwich recipe by the one and only David Masangcay'
            'is one of the greatest recipes ever created.  This recipe is used by almost all American families to'
            ' ensure their kids enjoy a delicious meal at a low price!',
        softWrap: true,),
    );

    return MaterialApp(
      title: 'Homework 2',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Famous Recipes'),
        ),
        body: ListView(
            children: [
              Image.asset(
                'images/pbj.png', width: 650, height: 240, fit: BoxFit.cover,),
              titleSection,
              buttonSection,
              textSection,
              FavoriteWidget(),
            ]
        ),
      ),
    );
  }
}

class FavoriteWidget extends StatefulWidget {
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool _isFavorited = true;
  int _favoriteCount = 24;
  @override

  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isFavorited ? Icon(Icons.thumb_up) : Icon(Icons.favorite_border)),
            color: Colors.blue[500],
            onPressed: _toggleFavorite,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
            child:Text('$_favoriteCount'),
          ),
        )
      ],
    );
  }

  void _toggleFavorite() {
    setState(() {
      if(_isFavorited) {
        _favoriteCount += 1;
        _isFavorited = true;
      }

      else {
        _favoriteCount -= 1;
        _isFavorited = false;
      }
    });
  }
}

Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }